const express  = require('express'); 
const session = require('express-session'); 
const MongoStore = require('connect-mongo'); 

const app = express(); 

app.use(session({ 
    name: 'example.sid', 
    secret: 'Replace with your secret key', 
    httpOnly: true, 
    secure: true, 
    maxAge: 1000 * 60 * 60 * 7, 
    resave: false, 
    saveUninitialized: true , 
    store: MongoStore. create ({ 
        mongoUrl: 'mongodb://localhost:27017' 
    }) 
})); 

app.get('/', function (req, res) {

    console.log(req.session);

    req.session.numberOfRequests = req.session.numberOfRequests + 1;

    var requestCount = () => {
        return isNaN(req.session.numberOfRequests) ? 0 : req.session.numberOfRequests;
    };
    
    res.end('Number of reguests: ' + requestCount() +
        ' \n\r Refresh the page to increase count');
});

app.listen(4000, () => { 
    console.log("Програма прослуховує порт 4000") 
});
/*app.post('/', async (req, res, next) => { 
    const {name} = req.body; 
    req.session.user = { 
        name, 
        isLoggedIn: true 
    } 

    try { 
        await req.session.save( ); 
    } catch (err) { 
        console .error('Помилка збереження до сховища сеансу: ', err); 
        return next(new Error('Помилка створення користувача')); 
    } 

    res.status(200).send(); 
});

app.post('/logout', async (req, res, next) => { 
    try { 
        await req.session.destroy(); 
    } catch (err) { 
        console .error('Помилка виходу:', err) ; 
        return next(new Error('Error logging out')); 
    } 
    
    res.status(200).send(); 
});

app.get('/name', (req, res) => { 
    let name; 

    if (!req.session) { 
        return res.status(404).send(); 
    } 

    name = req.session.user.name ; 

    return res.status(200).send({name}); 
});*/
