const express = require('express');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);

var port = 8080; 

const app = express();

const options = {                 // setting connection options
    host: 'localhost',
    user: 'test',
    password: '1234512345',
    database: 'my_database',
    createDatabaseTable: false,
   };

const sessionStore = new MySQLStore(options);

app.use(
    session({
        secret: 'cookie_secret',
        resave: false,
        saveUninitialized: false,
        store: sessionStore,      // assigning sessionStore to the session
    })
);

app.get('/', function (req, res) {

    console.log(req.session);

    req.session.numberOfRequests = req.session.numberOfRequests + 1;

    var requestCount = () => {
        return isNaN(req.session.numberOfRequests) ? 0 : req.session.numberOfRequests;
    };
    
    res.end('Number of reguests: ' + requestCount() +
        ' \n\r Refresh the page to increase count');
})

app.listen(port, function () {
    console.log('app running on port ' + port);
})